<?php
	function iteora_scripts() {
	    wp_enqueue_script( 'formstyler', get_template_directory_uri() . '/js/jquery.formstyler.min.js');
		wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/js/slick.min.js');
	    wp_enqueue_script( 'core', get_template_directory_uri() . '/js/core.js');  
		wp_enqueue_script( 'salvatore-masonry', get_template_directory_uri() . '/js/salvattore.min.js');

		wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css');
		wp_enqueue_style( 'style', get_stylesheet_uri());
	}    
 
	add_action( 'wp_enqueue_scripts', 'iteora_scripts' ); // На внешней части сайта (в теме оформления)	

	// Add Miniature
	add_theme_support( 'post-thumbnails' );

	//Регистрируем топ меню

	register_nav_menu('menu','Меню в шапке');

	// Team Field
	if(is_admin()) {
	    wp_enqueue_script('imagefield', get_template_directory_uri().'/imagefield.js');
	}

	if ( ! function_exists( 'team_field' ) ) {
	 
	// Опишем требуемый функционал
	    function team_field() {
	 
	        $labels = array(
	            'name'                => _x( 'Team', 'Post Type General Name', 'team_field' ),
	            'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'team_field' ),
	            'menu_name'           => __( 'Team', 'team_field' ),
	            'parent_item_colon'   => __( 'Родительский:', 'team_field' ),
	            'all_items'           => __( 'Все записи', 'team_field' ),
	            'view_item'           => __( 'Просмотреть', 'team_field' ),
	            'add_new_item'        => __( 'Добавить новую запись в Team', 'team_field' ),
	            'add_new'             => __( 'Добавить новую', 'team_field' ),
	            'edit_item'           => __( 'Редактировать запись', 'team_field' ),
	            'update_item'         => __( 'Обновить запись', 'team_field' ),
	            'search_items'        => __( 'Найти запись', 'team_field' ),
	            'not_found'           => __( 'Не найдено', 'team_field' ),
	            'not_found_in_trash'  => __( 'Не найдено в корзине', 'team_field' ),
	        );
	        $args = array(
	            'labels'              => $labels,
	            'supports'            => array( 'title', 'editor', 'excerpt', ),
	            'taxonomies'          => array( 'team_field_tax' ), // категории, которые мы создадим ниже
	            'public'              => true,
	            'menu_position'       => 5,
	            'menu_icon'           => 'dashicons-book',
	        );
	        register_post_type( 'red_book', $args );
	    }
	    add_action( 'init', 'team_field', 0 ); // инициализируем
	 
	}


	if ( ! function_exists( 'team_field_tax' ) ) {
	 
	// Опишем требуемый функционал
	    function team_field_tax() {
	 
	        $labels = array(
	            'name'                       => _x( 'Категории Team Field', 'Taxonomy General Name', 'Team Field' ),
	            'singular_name'              => _x( 'Категория Team Field', 'Taxonomy Singular Name', 'Team Field' ),
	            'menu_name'                  => __( 'Категории', 'Team Field' ),
	            'all_items'                  => __( 'Категории', 'Team Field' ),
	            'parent_item'                => __( 'Родительская категория Книги', 'Team Field' ),
	            'parent_item_colon'          => __( 'Родительская категория Книги:', 'Team Field' ),
	            'new_item_name'              => __( 'Новая категория', 'Team Field' ),
	            'add_new_item'               => __( 'Добавить новую категорию', 'Team Field' ),
	            'edit_item'                  => __( 'Редактировать категорию', 'Team Field' ),
	            'update_item'                => __( 'Обновить категорию', 'Team Field' ),
	            'search_items'               => __( 'Найти', 'Team Field' ),
	            'add_or_remove_items'        => __( 'Добавить или удалить категорию', 'Team Field' ),
	            'choose_from_most_used'      => __( 'Поиск среди популярных', 'Team Field' ),
	            'not_found'                  => __( 'Не найдено', 'Team Field' ),
	        );
	        $args = array(
	            'labels'                     => $labels,
	            'hierarchical'               => true,
	            'public'                     => true,
	        );
	        register_taxonomy( 'team_field_tax', array( 'Team Field' ), $args );
	 
	    }
	 
	    add_action( 'init', 'team_field_tax', 0 ); // инициализируем
	 
	}

	function team_field_meta_box() {  
	    add_meta_box(  
	        'team_field_meta_box', // Идентификатор(id)
	        'Team Field - дополнительная информация', // Заголовок области с мета-полями(title)
	        'show_my_tean_metabox', // Вызов(callback)
	        'red_book', // Где будет отображаться наше поле, в нашем случае в разделе Красная Книга
	        'normal',
	        'high');
	}  
	add_action('add_meta_boxes', 'team_field_meta_box'); // Запускаем функцию
	$meta_fields = array(
	    array(  
	        'label' => 'Description',  
	        'desc'  => 'Write a description',  
	        'id'    => 'descr_for_team_field',  
	        'type'  => 'textarea'
	    ),
	    array(  
	        'label' => 'Name and Surname',  
	        'desc'  => 'Write name and surname',  
	        'id'    => 'name_and_surname_for_team_field',  
	        'type'  => 'text'
	    ),
	    array(  
	        'label' => 'Position',  
	        'desc'  => 'Write a position',  
	        'id'    => 'position_for_team_field', // даем идентификатор.
	        'type'  => 'text'  // Указываем тип поля.
	    ),	
		array(
		    'name'  => 'Image',
		    'label' => 'Image',
		    'desc'  => 'Select an image',
		    'id'    => 'image',
		    'type'  => 'image'
		)
	);	
	// Вызов метаполей  
	function show_my_tean_metabox() {  
	global $meta_fields; // Обозначим наш массив с полями глобальным
	global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
	// Выводим скрытый input, для верификации. Безопасность прежде всего!
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
	 
	    // Начинаем выводить таблицу с полями через цикл
	    echo '<table class="form-table">';  
	    foreach ($meta_fields as $field) {  
	        // Получаем значение если оно есть для этого поля 
	        $meta = get_post_meta($post->ID, $field['id'], true);  
	        // Начинаем выводить таблицу
	        echo '<tr> 
	                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
	                <td>';  
	                switch($field['type']) {  
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'image':
					    $image = get_template_directory_uri().'/images/image.png'; 
					    echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
					    if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; }              
					    echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
					                <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
					                    <input class="custom_upload_image_button button" type="button" value="Выберите изображение" />
					                    <small> <a href="#" class="custom_clear_image_button">Убрать изображение</a></small>
					                    <br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
	                }
	        echo '</td></tr>';  
	    }  
	    echo '</table>'; 
	}

	// Пишем функцию для сохранения
	function save_my_meta_fields($post_id) {  
	    global $meta_fields;  // Массив с нашими полями
	 
	    // проверяем наш проверочный код 
	    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
	        return $post_id;  
	    // Проверяем авто-сохранение 
	    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
	        return $post_id;  
	    // Проверяем права доступа  
	    if ('page' == $_POST['post_type']) {  
	        if (!current_user_can('edit_page', $post_id))  
	            return $post_id;  
	        } elseif (!current_user_can('edit_post', $post_id)) {  
	            return $post_id;  
	    }  
	 
	    // Если все отлично, прогоняем массив через foreach
	    foreach ($meta_fields as $field) {  
	        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
	        $new = $_POST[$field['id']];  
	        if ($new && $new != $old) {  // Если данные новые
	            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
	        } elseif ('' == $new && $old) {  
	            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
	        }  
	    } // end foreach  
	}  
	add_action('save_post', 'save_my_meta_fields'); // Запускаем функцию сохранения*/



	/*if ( ! function_exists( 'project_field' ) ) {
	 
	// Опишем требуемый функционал
	    function project_field() {
	 
	        $labels = array(
	            'name'                => _x( 'Projects', 'Post Type General Name', 'project_field' ),
	            'singular_name'       => _x( 'Projects', 'Post Type Singular Name', 'project_field' ),
	            'menu_name'           => __( 'Projects', 'project_field' ),
	            'parent_item_colon'   => __( 'Родительский:', 'project_field' ),
	            'all_items'           => __( 'Все записи', 'project_field' ),
	            'view_item'           => __( 'Просмотреть', 'project_field' ),
	            'add_new_item'        => __( 'Добавить новую запись в Team', 'project_field' ),
	            'add_new'             => __( 'Добавить новую', 'project_field' ),
	            'edit_item'           => __( 'Редактировать запись', 'project_field' ),
	            'update_item'         => __( 'Обновить запись', 'project_field' ),
	            'search_items'        => __( 'Найти запись', 'project_field' ),
	            'not_found'           => __( 'Не найдено', 'project_field' ),
	            'not_found_in_trash'  => __( 'Не найдено в корзине', 'project_field' ),
	        );
	        $args = array(
	            'labels'              => $labels,
	            'supports'            => array( 'title', 'editor', 'excerpt', ),
	            'taxonomies'          => array( 'project_field_tax' ), // категории, которые мы создадим ниже
	            'public'              => true,
	            'menu_position'       => 5,
	            'menu_icon'           => 'dashicons-book',
	        );
	        register_post_type( 'project_field', $args );
	    }
	    add_action( 'init', 'project_field', 0 ); // инициализируем
	 
	}

	function project_field_meta_box() {  
	    add_meta_box(  
	        'project_field_meta_box', // Идентификатор(id)
	        'Project field - дополнительная информация', // Заголовок области с мета-полями(title)
	        'show_my_tean_metabox', // Вызов(callback)
	        'red_book', // Где будет отображаться наше поле, в нашем случае в разделе Красная Книга
	        'normal',
	        'high');
	}  
	add_action('add_meta_boxes', 'project_field_meta_box'); // Запускаем функцию
	$meta_fields = array(
	    array(  
	        'label' => 'Project name',  
	        'desc'  => 'Write a project name',  
	        'id'    => 'project_name',  
	        'type'  => 'text'
	    ),
	    array(  
	        'label' => 'Project description',  
	        'desc'  => 'Write project\'s description',  
	        'id'    => 'project_description',  
	        'type'  => 'textarea'
	    ),
	    array(  
	        'label' => 'Position',  
	        'desc'  => 'Write a position',  
	        'id'    => 'position_for_team_field', // даем идентификатор.
	        'type'  => 'text'  // Указываем тип поля.
	    ),	
		array(
		    'name'  => 'Image',
		    'label' => 'Image',
		    'desc'  => 'Select an image',
		    'id'    => 'image',
		    'type'  => 'image'
		)
	);	
	// Вызов метаполей  
	function show_my_tean_metabox() {  
	global $meta_fields; // Обозначим наш массив с полями глобальным
	global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
	// Выводим скрытый input, для верификации. Безопасность прежде всего!
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
	 
	    // Начинаем выводить таблицу с полями через цикл
	    echo '<table class="form-table">';  
	    foreach ($meta_fields as $field) {  
	        // Получаем значение если оно есть для этого поля 
	        $meta = get_post_meta($post->ID, $field['id'], true);  
	        // Начинаем выводить таблицу
	        echo '<tr> 
	                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
	                <td>';  
	                switch($field['type']) {  
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'image':
					    $image = get_template_directory_uri().'/images/image.png'; 
					    echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
					    if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; }              
					    echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
					                <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
					                    <input class="custom_upload_image_button button" type="button" value="Выберите изображение" />
					                    <small> <a href="#" class="custom_clear_image_button">Убрать изображение</a></small>
					                    <br clear="all" /><span class="description">'.$field['desc'].'</span>';
					break;
	                }
	        echo '</td></tr>';  
	    }  
	    echo '</table>'; 
	}

	// Пишем функцию для сохранения
	function save_my_meta_fields($post_id) {  
	    global $meta_fields;  // Массив с нашими полями
	 
	    // проверяем наш проверочный код 
	    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
	        return $post_id;  
	    // Проверяем авто-сохранение 
	    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
	        return $post_id;  
	    // Проверяем права доступа  
	    if ('page' == $_POST['post_type']) {  
	        if (!current_user_can('edit_page', $post_id))  
	            return $post_id;  
	        } elseif (!current_user_can('edit_post', $post_id)) {  
	            return $post_id;  
	    }  
	 
	    // Если все отлично, прогоняем массив через foreach
	    foreach ($meta_fields as $field) {  
	        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
	        $new = $_POST[$field['id']];  
	        if ($new && $new != $old) {  // Если данные новые
	            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
	        } elseif ('' == $new && $old) {  
	            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
	        }  
	    } // end foreach  
	}  
	add_action('save_post', 'save_my_meta_fields'); // Запускаем функцию сохранения*/












		/*function my_meta_box() {  
	    add_meta_box(  
	        'my_meta_box', // Идентификатор(id)
	        'My Meta Box', // Заголовок области с мета-полями(title)
	        'show_my_metabox', // Вызов(callback)
	        'post', // Где будет отображаться наше поле, в нашем случае в Записях
	        'normal', 
	        'high');
	}  
	add_action('add_meta_boxes', 'my_meta_box'); // Запускаем функцию

	$meta_fields = array(  
	    array(  
	        'label' => 'Description',  
	        'desc'  => 'Write a description',  
	        'id'    => 'descr_for_team_field',  
	        'type'  => 'textarea'
	    ),
	    array(  
	        'label' => 'Name and Surname',  
	        'desc'  => 'Write name and surname',  
	        'id'    => 'name_and_surname_for_team_field',  
	        'type'  => 'text'
	    ),
	    array(  
	        'label' => 'Position',  
	        'desc'  => 'Write a position',  
	        'id'    => 'position_for_team_field', // даем идентификатор.
	        'type'  => 'text'  // Указываем тип поля.
	    ),	
		array(
		    'name'  => 'Image',
		    'label' => 'Image',
		    'desc'  => 'Select an image',
		    'id'    => 'image',
		    'type'  => 'image'
		)
	);

	// Вызов метаполей  
	function show_my_metabox() {  
	global $meta_fields; // Обозначим наш массив с полями глобальным
	global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
	// Выводим скрытый input, для верификации. Безопасность прежде всего!
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
	 
	    // Начинаем выводить таблицу с полями через цикл
	    echo '<table class="form-table">';  
	    foreach ($meta_fields as $field) {  
	        // Получаем значение если оно есть для этого поля 
	        $meta = get_post_meta($post->ID, $field['id'], true);  
	        // Начинаем выводить таблицу
	        echo '<tr> 
	                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
	                <td>';  
	                switch($field['type']) {  
	                    case 'text':  
						    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
						        <br /><span class="description">'.$field['desc'].'</span>';  
						break;
						case 'textarea':  
						    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
						        <br /><span class="description">'.$field['desc'].'</span>';  
						break;
						case 'image':
						    $image = get_template_directory_uri().'/img/team-img.png'; 
						    echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
						    if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; }              
						    echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
						                <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
						                    <input class="custom_upload_image_button button" type="button" value="Выберите изображение" />
						                    <small> <a href="#" class="custom_clear_image_button">Убрать изображение</a></small>
						                    <br clear="all" /><span class="description">'.$field['desc'].'</span>';
						break;
	                }
	        echo '</td></tr>';  
	    }  
	    echo '</table>'; 
	}

		// Пишем функцию для сохранения
	function save_my_meta_fields($post_id) {  
	    global $meta_fields;  // Массив с нашими полями
	 
	    // проверяем наш проверочный код 
	    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
	        return $post_id;  
	    // Проверяем авто-сохранение 
	    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
	        return $post_id;  
	    // Проверяем права доступа  
	    if ('page' == $_POST['post_type']) {  
	        if (!current_user_can('edit_page', $post_id))  
	            return $post_id;  
	        } elseif (!current_user_can('edit_post', $post_id)) {  
	            return $post_id;  
	    }  
	 
	    // Если все отлично, прогоняем массив через foreach
	    foreach ($meta_fields as $field) {  
	        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
	        $new = $_POST[$field['id']];  
	        if ($new && $new != $old) {  // Если данные новые
	            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
	        } elseif ('' == $new && $old) {  
	            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
	        }  
	    } // end foreach  
	}  
	add_action('save_post', 'save_my_meta_fields'); // Запускаем функцию сохранения

	*/
?>