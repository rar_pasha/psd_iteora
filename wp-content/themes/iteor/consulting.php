<?php
/*
Template Name: It consulting
*/
?>
    <div class="consulting-nav">
        <?php get_header(); ?>  
    </div>

        <?php include('contactpopup.php'); ?> 

        <div class="wrapper">
            <div class="header-wrapper consulting-back">
                <div class="header center">
                    <?php 
                        query_posts(array( 
                            'post_type' => 'cons_header_content',
                            'showposts' => 1 
                        ) );  
                    ?>


                    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

                        <div class="consulting-header">
                            <h1><?php 
                                $h1 = get_post_meta($post->ID, 'h1', true);
                                echo $h1;
                                ?></h1>
                            <h3><?php 
                                $h2 = get_post_meta($post->ID, 'h2', true);
                                echo $h2;
                                ?></h3>
                            <div class="startup-btn-group consulting-header-btn">
                                <a href="#" class="get-quote-link">Get a Qoute</a>
                            </div>   
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?> 
                </div>
            </div>
            

            <div class="consulting-services-wrapper">
                <div class="consulting-services center">
                    <h2 class="standart-header">services</h2>
                    <div class="consulting-services-content" data-columns>
                        <div class="consulting-services-content-block consulting-services-content-block1">
                            <h3>Full Implementations and Custom Development</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block2">
                            <h3>Market Place Analysis and Advice</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block3">
                            <h3>Big Data Assessment and Strategy</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block4">
                            <h3>Center of Excellence/Support Model Setup</h3>
                        </div>

                        <div class="consulting-services-content-block consulting-services-content-block5">
                            <h3>Use Case Development</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block6">
                            <h3>Planning, Oversight, and Validation</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block7">
                            <h3>Organization Assessment and Design</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block8">
                            <h3>Project/Program Reviews</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block9">
                            <h3>Architecture Review</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block10">
                            <h3>Use Case Reviews</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block11">
                            <h3>Data Integration</h3>
                        </div>
                        <div class="consulting-services-content-block consulting-services-content-block12">
                            <h3>Data Driven Marketing/Advertising Assessment</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about-companys-achievments-wrapper">
                <div class="about-companys-achievments center body-leasing-consultation">
                    <h2>Need some consultation? </h2>
                    <a href="#">Get in touch now!</a>
                </div>
            </div>


            <div class="industries-wrapper">
                <div class="industries-content center">
                    <h2 class="standart-header">Industries</h2>
                    <div class="industries"></div>
                </div>
            </div>
            
            <div class="cons-projects">
                <?php include('/projects.php') ?>   
            </div>


            <div class="team-wrapper">
                <div class="team center" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 4 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>
                </div>
            </div>

            <?php include('contact-us.php'); ?> 

    <?php get_footer(); ?>