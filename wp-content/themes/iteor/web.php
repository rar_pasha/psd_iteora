<?php
/*
Template Name: Web
*/
?>
	<div class="web-nav">
    	<?php get_header(); ?>	
	</div>

        <?php include('contactpopup.php'); ?> 


        <div class="wrapper">
            <div class="header-wrapper web-back">
                <div class="header center"> 
                  <div class="header-content">
						
						<?php 
	                        query_posts(array( 
	                            'post_type' => 'web_header_content',
	                            'showposts' => 7 
	                        ) );  
	                    ?>


	                    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

	                	<div class="web-header">
					        <div class="web-header-left">
					            <?php the_post_thumbnail(); ?>
					        </div>
					        <div class="web-header-right">
					            <h1><?php 
	                       			 $h1 = get_post_meta($post->ID, 'h1', true);
	                        		 echo $h1;
	                        	?></h1>
					            <h5><?php 
	                        	$h2 = get_post_meta($post->ID, 'h2', true);
	                        	echo $h2;
	                        	?></h5> 
					            <a href="#" class="get-quote-link">Get a quote</a> 
					        </div>
					    </div>
					    <?php endwhile; ?>
	                    <!-- post navigation -->
	                    <?php else: ?>
	                    <!-- no posts found -->
	                    <?php endif; ?>
 
				  </div>
                </div>
            </div>
			<div class="web-tech center">
            	<?php include('ourtech.php'); ?> 	
			</div>

			<div class="how-we-work-wrapper">
				<div class="how-we-work center">
					<h2>How we work</h2>
					<div class="how-we-work-blocks">
						<div class="how-we-work-block build-project build-project-clicked col-md-4 col-xs-offset-2 how-clicked" data-tab="1">
							<h4>Build your project</h4>
						</div>
						<div class="how-we-work-block build-team col-md-4" data-tab="2">
							<h4>Build your Team</h4>
						</div>
					</div>
					<div class="how-we-works-tabs">
						<div class="how-we-works-tab" data-tab="1">
							<div class="build-team-punkts">
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>1</span>
									</div>
									<h3>Identify your needs</h3>
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>2</span>
									</div>
									<h3>Execute business analysis and present a proposal</h3>	
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>3</span>
									</div>
									<h3>Negotiate price terms and timeframe</h3>
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>4</span>
									</div>
									<h3>Project roadmap</h3>
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>5</span>
									</div>
									<h3>Project management</h3>
								</div>
							</div>
						</div>
						<div class="how-we-works-tab"  data-tab="2">
						<div class="build-team-punkts">
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>1</span>
									</div>
									<h3>Identify your needs</h3>
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>2</span>
									</div>
									<h3>Search for profiles in our database and send you CVs</h3>
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>3</span>
									</div>
									<h3>Negotiate price terms</h3>	
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>4</span>
									</div>
									<h3>Arrange meeting</h3>	
								</div>
								<div class="build-team-punkt">
									<div class="build-team-punkt-top">
										<span>5</span>
									</div>
									<h3>Organize workplace and communication</h3>
								</div>
						</div>
						</div>
					</div>
				</div>
			</div>


			<div class="about-companys-achievments-wrapper">
				<div class="about-companys-achievments center body-leasing-consultation">
					<h2>Need some consultation? </h2>
					<a href="#" class="contact-popup-link">Get in touch now!</a>
				</div>
			</div>

			<div class="web-our-team">
            	<?php include('our-team.php') ?>			
			</div>



			<?php include('projects.php') ?>

			<div class="industries-wrapper">
				<div class="industries-content center">
					<h2 class="standart-header">Industries</h2>
					<div class="industries"></div>
				</div>
			</div>

            <div class="team-wrapper">
                <div class="team center" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 2 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>
            	</div>
            </div>

            <?php include('contact-us.php'); ?> 
            
    <?php get_footer(); ?>