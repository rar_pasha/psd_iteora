<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
add_filter('show_admin_bar', '__return_false');
/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'iteor_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own iteor_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function iteor_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/iteor
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'iteor' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'iteor' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'iteor' ),
		'social'  => __( 'Social Links Menu', 'iteor' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', iteor_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // iteor_setup
add_action( 'after_setup_theme', 'iteor_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function iteor_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'iteor_content_width', 840 );
}
add_action( 'after_setup_theme', 'iteor_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function iteor_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'iteor' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'iteor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 1', 'iteor' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'iteor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'iteor' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'iteor' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'iteor_widgets_init' );

if ( ! function_exists( 'iteor_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own iteor_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function iteor_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'iteor' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'iteor' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'iteor' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function iteor_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'iteor_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function iteor_scripts() {
	/*
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'iteor-fonts', iteor_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'iteor-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'iteor-ie', get_template_directory_uri() . '/css/ie.css', array( 'iteor-style' ), '20160816' );
	wp_style_add_data( 'iteor-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'iteor-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'iteor-style' ), '20160816' );
	wp_style_add_data( 'iteor-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'iteor-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'iteor-style' ), '20160816' );
	wp_style_add_data( 'iteor-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'iteor-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'iteor-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'iteor-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160816', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'iteor-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
	}

	wp_enqueue_script( 'iteor-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', true );

	wp_localize_script( 'iteor-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'iteor' ),
		'collapse' => __( 'collapse child menu', 'iteor' ),
	) );
*/


	/*My scripts*/
    wp_enqueue_script( 'formstyler', get_template_directory_uri() . '/js/jquery.formstyler.min.js');
	wp_enqueue_script( 'slick-slider', get_template_directory_uri() . '/js/slick.min.js');
    wp_enqueue_script( 'core', get_template_directory_uri() . '/js/core.js');  
	wp_enqueue_script( 'salvatore-masonry', get_template_directory_uri() . '/js/salvattore.min.js');
	wp_enqueue_script( 'salvatore-masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js');

	wp_enqueue_style( 'slick-css', get_template_directory_uri() . '/css/slick.css');
	wp_enqueue_style( 'startup', get_template_directory_uri() . '/css/startup.css');
	wp_enqueue_style( 'mobile', get_template_directory_uri() . '/css/mobile.css');
	wp_enqueue_style( 'bode-leasing', get_template_directory_uri() . '/css/body-leasing.css');
	wp_enqueue_style( 'web', get_template_directory_uri() . '/css/web.css');
	wp_enqueue_style( 'consulting', get_template_directory_uri() . '/css/consulting.css');
	wp_enqueue_style( 'cases', get_template_directory_uri() . '/css/cases.css');
	wp_enqueue_style( 'style', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'iteor_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function iteor_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'iteor_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function iteor_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function iteor_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'iteor_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function iteor_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'iteor_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function iteor_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'iteor_widget_tag_cloud_args' );










if ( ! function_exists( 'team' ) ) {
 
// Опишем требуемый функционал
    function team() {
 
        $labels = array(
            'name'                => _x( 'Our Team', 'Post Type General Name', 'team' ),
            'singular_name'       => _x( 'Our Team', 'Post Type Singular Name', 'team' ),
            'all_items'           => __( 'Все записи', 'team' ),
            'view_item'           => __( 'Просмотреть', 'team' ),
            'add_new_item'        => __( 'Укажите имя, фамилию, описание и миниатюру для our team', 'team' ),
            'add_new'             => __( 'Добавить новую', 'team' ),
            'edit_item'           => __( 'Редактировать запись', 'team' ),
            'update_item'         => __( 'Обновить запись', 'team' ),
            'search_items'        => __( 'Найти запись', 'team' ),
            'not_found'           => __( 'Не найдено', 'team' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'team' ),
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-book',
        );
        register_post_type( 'team', $args );
 
    }
 
    add_action( 'init', 'team', 0 ); // инициализируем
 
}

function team_func() {  
    add_meta_box(  
        'my_team', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_team', // Вызов(callback)
        'team', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'team_func');

$team_meta_fields = array(  
    array(  
        'label' => 'Позиция',  
        'desc'  => 'Позиция для our team',  
        'id'    => 'team_position', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    ),
    array(
        'label' => 'Ссылка на gitHub',  
        'desc'  => 'Введите ссылку на gitHub',  
        'id'    => 'gitHub_link', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Ссылка на in',  
        'desc'  => 'Введите ссылку на in',  
        'id'    => 'in_link', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Опыт',  
        'desc'  => 'Укажите опыт работы',  
        'id'    => 'team_exp', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_team() {  
global $team_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($team_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}




if ( ! function_exists( 'projects' ) ) {
 
// Опишем требуемый функционал
    function projects() {
 
        $labels = array(
            'name'                => _x( 'Projects', 'Post Type General Name', 'projects' ),
            'singular_name'       => _x( 'Projects', 'Post Type Singular Name', 'projects' ),
            'all_items'           => __( 'Все записи', 'projects' ),
            'view_item'           => __( 'Просмотреть', 'projects' ),
            'add_new_item'        => __( 'Укажи название проекта, описание, категорию и задайте миниатюру', 'projects' ),
            'add_new'             => __( 'Добавить новую', 'projects' ),
            'edit_item'           => __( 'Редактировать запись', 'projects' ),
            'update_item'         => __( 'Обновить запись', 'projects' ),
            'search_items'        => __( 'Найти запись', 'projects' ),
            'not_found'           => __( 'Не найдено', 'projects' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'projects' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'projects', $args );
 
    }
 
    add_action( 'init', 'projects', 0 ); // инициализируем
 
}

function projects_func() {  
    add_meta_box(  
        'my_project', // Идентификатор(id)
        'Укажите категорию', // Заголовок области с мета-полями(title)
        'show_my_project', // Вызов(callback)
        'projects', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'projects_func');

$meta_fields = array(   
    array(  
        'label' => 'Категория проекта',  
        'desc'  => 'укажите категорию данного проекта',  
        'id'    => 'category',  
        'type'  => 'select',  
        'options' => array (  // Параметры, всплывающие данные
            'Web_development' => array (  
                'label' => 'Web Development',  // Название поля
                'value' => 'Web Development'  // Значение
            ),  
            'two' => array (  
                'label' => 'Mobile Development',  // Название поля
                'value' => 'Mobile Development'  // Значение
            )  
        )
    )
);

function show_my_project() {  
global $meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}






if ( ! function_exists( 'gallery' ) ) {
 
// Опишем требуемый функционал
    function gallery() {
 
        $labels = array(
            'name'                => _x( 'Photo Gallery', 'Post Type General Name', 'gallery' ),
            'singular_name'       => _x( 'Projects', 'Post Type Singular Name', 'gallery' ),
            'all_items'           => __( 'Все записи', 'gallery' ),
            'view_item'           => __( 'Просмотреть', 'gallery' ),
            'add_new_item'        => __( 'Задайте миниатюру и название картинки, описание(не обязательно)', 'gallery' ),
            'add_new'             => __( 'Добавить новую', 'gallery' ),
            'edit_item'           => __( 'Редактировать запись', 'gallery' ),
            'update_item'         => __( 'Обновить запись', 'gallery' ),
            'search_items'        => __( 'Найти запись', 'gallery' ),
            'not_found'           => __( 'Не найдено', 'gallery' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'gallery' ),
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-book',
        );
        register_post_type( 'gallery', $args );
 
    }
 
    add_action( 'init', 'gallery', 0 ); // инициализируем
 
}









// Пишем функцию для сохранения
function save_my_team_meta_fields($post_id) {  
    global $team_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($team_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_team_meta_fields'); // Запускаем функцию сохранения


// Пишем функцию для сохранения
function save_my_meta_fields($post_id) {  
    global $meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_meta_fields'); // Запускаем функцию сохранения




    if ( ! function_exists( 'main_header_content' ) ) {
 
// Опишем требуемый функционал
    function main_header_content() {
 
        $labels = array(
            'name'                => _x( 'Main Header Content', 'Post Type General Name', 'main_header_content' ),
            'singular_name'       => _x( 'Header content', 'Post Type Singular Name', 'main_header_content' ),
            'all_items'           => __( 'Все записи', 'main_header_content' ),
            'view_item'           => __( 'Просмотреть', 'main_header_content' ),
            'add_new_item'        => __( 'Укажи данные для слайдера на главной странице (обязательно заполните нижнее поле)', 'main_header_content' ),
            'add_new'             => __( 'Добавить новую', 'main_header_content' ),
            'edit_item'           => __( 'Редактировать запись', 'main_header_content' ),
            'update_item'         => __( 'Обновить запись', 'main_header_content' ),
            'search_items'        => __( 'Найти запись', 'main_header_content' ),
            'not_found'           => __( 'Не найдено', 'main_header_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'main_header_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'main_header_content', $args );
 
    }
 
    add_action( 'init', 'main_header_content', 0 ); // инициализируем
 
}

function main_head_func() {  
    add_meta_box(  
        'my_main_head', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_main_head', // Вызов(callback)
        'main_header_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'main_head_func');

$main_head_meta_fields = array(   
    array(
        'label' => 'Заголовок первого уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h1', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Заголовок второго уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h2', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Заголовок третьего уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h3', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_main_head() {  
global $main_head_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($main_head_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_main_head_meta_fields($post_id) {  
    global $main_head_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($main_head_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_main_head_meta_fields'); // Запускаем функцию сохранения






    if ( ! function_exists( 'about_header_content' ) ) {
 
// Опишем требуемый функционал
    function about_header_content() {
 
        $labels = array(
            'name'                => _x( 'About us Header Content', 'Post Type General Name', 'about_header_content' ),
            'singular_name'       => _x( 'About Header content', 'Post Type Singular Name', 'about_header_content' ),
            'all_items'           => __( 'Все записи', 'about_header_content' ),
            'view_item'           => __( 'Просмотреть', 'about_header_content' ),
            'add_new_item'        => __( 'Укажи данные для слайдера на About us странице (обязательно заполните нижнее поле)', 'about_header_content' ),
            'add_new'             => __( 'Добавить новую', 'about_header_content' ),
            'edit_item'           => __( 'Редактировать запись', 'about_header_content' ),
            'update_item'         => __( 'Обновить запись', 'about_header_content' ),
            'search_items'        => __( 'Найти запись', 'about_header_content' ),
            'not_found'           => __( 'Не найдено', 'about_header_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'about_header_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'about_header_content', $args );
 
    }
 
    add_action( 'init', 'about_header_content', 0 ); // инициализируем
 
}

function about_head_func() {  
    add_meta_box(  
        'my_about_head', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_about_head', // Вызов(callback)
        'about_header_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'about_head_func');

$about_head_meta_fields = array(   
    array(
        'label' => 'Заголовок первого уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h1', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Заголовок второго уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h2', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Заголовок третьего уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h3', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_about_head() {  
global $about_head_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($about_head_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_about_head_about_fields($post_id) {  
    global $about_head_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($about_head_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_about_head_about_fields'); // Запускаем функцию сохранения








    if ( ! function_exists( 'web_header_content' ) ) {
 
// Опишем требуемый функционал
    function web_header_content() {
 
        $labels = array(
            'name'                => _x( 'Web Header Content', 'Post Type General Name', 'web_header_content' ),
            'singular_name'       => _x( 'Web Header content', 'Post Type Singular Name', 'web_header_content' ),
            'all_items'           => __( 'Все записи', 'web_header_content' ),
            'view_item'           => __( 'Просмотреть', 'web_header_content' ),
            'add_new_item'        => __( 'Укажи данные для слайдера на Web странице (обязательно заполните нижнее поле)', 'web_header_content' ),
            'add_new'             => __( 'Добавить новую', 'web_header_content' ),
            'edit_item'           => __( 'Редактировать запись', 'web_header_content' ),
            'update_item'         => __( 'Обновить запись', 'web_header_content' ),
            'search_items'        => __( 'Найти запись', 'web_header_content' ),
            'not_found'           => __( 'Не найдено', 'web_header_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'web_header_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'web_header_content', $args );
 
    }
 
    add_action( 'init', 'web_header_content', 0 ); // инициализируем
 
}

function web_head_func() {  
    add_meta_box(  
        'my_web_head', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_web_head', // Вызов(callback)
        'web_header_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'web_head_func');

$web_head_meta_fields = array(   
    array(
        'label' => 'Заголовок первого уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h1', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Текст второго уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h2', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_web_head() {  
global $web_head_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($web_head_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_web_head_about_fields($post_id) {  
    global $web_head_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($web_head_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_web_head_about_fields'); // Запускаем функцию сохранения











    if ( ! function_exists( 'mob_header_content' ) ) {
 
// Опишем требуемый функционал
    function mob_header_content() {
 
        $labels = array(
            'name'                => _x( 'Mobile Header Content', 'Post Type General Name', 'mob_header_content' ),
            'singular_name'       => _x( 'Mobile Header content', 'Post Type Singular Name', 'mob_header_content' ),
            'all_items'           => __( 'Все записи', 'mob_header_content' ),
            'view_item'           => __( 'Просмотреть', 'mob_header_content' ),
            'add_new_item'        => __( 'Укажи данные для слайдера на странице Mobile (обязательно заполните нижнее поле)', 'mob_header_content' ),
            'add_new'             => __( 'Добавить новую', 'mob_header_content' ),
            'edit_item'           => __( 'Редактировать запись', 'mob_header_content' ),
            'update_item'         => __( 'Обновить запись', 'mob_header_content' ),
            'search_items'        => __( 'Найти запись', 'mob_header_content' ),
            'not_found'           => __( 'Не найдено', 'mob_header_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'mob_header_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'mob_header_content', $args );
 
    }
 
    add_action( 'init', 'mob_header_content', 0 ); // инициализируем
 
}

function mob_head_func() {  
    add_meta_box(  
        'my_mob_head', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_mob_head', // Вызов(callback)
        'mob_header_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'mob_head_func');

$mob_head_meta_fields = array(   
    array(
        'label' => 'Заголовок первого уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h1', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Текст второго уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h2', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_mob_head() {  
global $mob_head_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($mob_head_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_mob_fields($post_id) {  
    global $mob_head_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($mob_head_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_mob_fields'); // Запускаем функцию сохранения











    if ( ! function_exists( 'cons_header_content' ) ) {
 
// Опишем требуемый функционал
    function cons_header_content() {
 
        $labels = array(
            'name'                => _x( 'It consulting Header', 'Post Type General Name', 'cons_header_content' ),
            'singular_name'       => _x( 'It consulting Header', 'Post Type Singular Name', 'cons_header_content' ),
            'all_items'           => __( 'Все записи', 'cons_header_content' ),
            'view_item'           => __( 'Просмотреть', 'cons_header_content' ),
            'add_new_item'        => __( 'Укажи данные для верхней части блока it consulting', 'cons_header_content' ),
            'add_new'             => __( 'Добавить новую', 'cons_header_content' ),
            'edit_item'           => __( 'Редактировать запись', 'cons_header_content' ),
            'update_item'         => __( 'Обновить запись', 'cons_header_content' ),
            'search_items'        => __( 'Найти запись', 'cons_header_content' ),
            'not_found'           => __( 'Не найдено', 'cons_header_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'cons_header_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 6,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'cons_header_content', $args );
 
    }
 
    add_action( 'init', 'cons_header_content', 0 ); // инициализируем
 
}

function cons_head_func() {  
    add_meta_box(  
        'my_cons_head', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_cons_head', // Вызов(callback)
        'cons_header_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'cons_head_func');

$cons_head_meta_fields = array(   
    array(
        'label' => 'Заголовок первого уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h1', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    ),
    array(
        'label' => 'Текст второго уровня',  
        'desc'  => 'Укажите значение',  
        'id'    => 'h2', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.	
    )
);

function show_my_cons_head() {  
global $cons_head_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($cons_head_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_cons_fields($post_id) {  
    global $cons_head_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($cons_head_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_cons_fields'); // Запускаем функцию сохранения






    if ( ! function_exists( 'reviews_content' ) ) {
 
// Опишем требуемый функционал
    function reviews_content() {
 
        $labels = array(
            'name'                => _x( 'Reviews content', 'Post Type General Name', 'reviews_content' ),
            'singular_name'       => _x( 'Reviews content', 'Post Type Singular Name', 'reviews_content' ),
            'all_items'           => __( 'Все записи', 'reviews_content' ),
            'view_item'           => __( 'Просмотреть', 'reviews_content' ),
            'add_new_item'        => __( 'Укажи отзыви', 'reviews_content' ),
            'add_new'             => __( 'Добавить новую', 'reviews_content' ),
            'edit_item'           => __( 'Редактировать запись', 'reviews_content' ),
            'update_item'         => __( 'Обновить запись', 'reviews_content' ),
            'search_items'        => __( 'Найти запись', 'reviews_content' ),
            'not_found'           => __( 'Не найдено', 'reviews_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'reviews_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 1,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'reviews_content', $args );
 
    }
 
    add_action( 'init', 'reviews_content', 0 ); // инициализируем
 
}

function rev_func() {  
    add_meta_box(  
        'my_revs', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_revs', // Вызов(callback)
        'reviews_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'rev_func');

$revs_meta_fields = array(   
    array(  
        'label' => 'Должность',  
        'desc'  => 'укажите должность',  
        'id'    => 'revs_position', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    )
);

function show_my_revs() {  
global $revs_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($revs_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_revs_fields($post_id) {  
    global $revs_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($revs_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_revs_fields'); // Запускаем функцию сохранения







    if ( ! function_exists( 'startups_content' ) ) {
 
// Опишем требуемый функционал
    function startups_content() {
 
        $labels = array(
            'name'                => _x( 'Startup Header', 'Post Type General Name', 'startups_content' ),
            'singular_name'       => _x( 'Startup Header', 'Post Type Singular Name', 'startups_content' ),
            'all_items'           => __( 'Все записи', 'startups_content' ),
            'view_item'           => __( 'Просмотреть', 'startups_content' ),
            'add_new_item'        => __( 'Укажи данные для верхнего описания блока Startup', 'startups_content' ),
            'add_new'             => __( 'Добавить новую', 'startups_content' ),
            'edit_item'           => __( 'Редактировать запись', 'startups_content' ),
            'update_item'         => __( 'Обновить запись', 'startups_content' ),
            'search_items'        => __( 'Найти запись', 'startups_content' ),
            'not_found'           => __( 'Не найдено', 'startups_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'startups_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 1,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'startups_content', $args );
 
    }
 
    add_action( 'init', 'startups_content', 6 ); // инициализируем
 
}




    if ( ! function_exists( 'case_content' ) ) {
 
// Опишем требуемый функционал
    function case_content() {
 
        $labels = array(
            'name'                => _x( 'Case Studies', 'Post Type General Name', 'case_content' ),
            'singular_name'       => _x( 'Case Studies', 'Post Type Singular Name', 'case_content' ),
            'all_items'           => __( 'Все записи', 'case_content' ),
            'view_item'           => __( 'Просмотреть', 'case_content' ),
            'add_new_item'        => __( 'Укажи данные для case studies (Первое поле это название, второе - описание)', 'case_content' ),
            'add_new'             => __( 'Добавить новую', 'case_content' ),
            'edit_item'           => __( 'Редактировать запись', 'case_content' ),
            'update_item'         => __( 'Обновить запись', 'case_content' ),
            'search_items'        => __( 'Найти запись', 'case_content' ),
            'not_found'           => __( 'Не найдено', 'case_content' ),
            'not_found_in_trash'  => __( 'Не найдено в корзине', 'case_content' )
        );
        $args = array(
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail'),
            'public'              => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-book'
        );
        register_post_type( 'case_content', $args );
 
    }
 
    add_action( 'init', 'case_content', 0 ); // инициализируем
 
}

function case_func() {  
    add_meta_box(  
        'case_func', // Идентификатор(id)
        'Укажите данные', // Заголовок области с мета-полями(title)
        'show_my_case', // Вызов(callback)
        'case_content', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal', 
        'high');
}  
add_action('add_meta_boxes', 'case_func');

$case_meta_fields = array(   
	array(  
        'label' => 'Описание проблемы',  
        'desc'  => 'укажите описание',  
        'id'    => 'case_problem', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Заголовок первого шага решения проблемы',  
        'desc'  => 'Опишите заголовок первого шага решения проблемы',  
        'id'    => 'res1_h', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Описание первого шага решения проблемы',  
        'desc'  => 'укажите описание первого шага решения проблемы',  
        'id'    => 'res1_descr', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Заголовок второго шага решения проблемы',  
        'desc'  => 'Опишите заголовок второго шага решения проблемы',  
        'id'    => 'res2_h', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Описание второго шага решения проблемы',  
        'desc'  => 'укажите описание второго шага решения проблемы',  
        'id'    => 'res2_descr', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Заголовок третьего шага решения проблемы',  
        'desc'  => 'Опишите заголовок третьего шага решения проблемы',  
        'id'    => 'res3_h', // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Описание третьего шага решения проблемы',  
        'desc'  => 'укажите описание третьего шага решения проблемы',  
        'id'    => 'res3_descr', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Первый заголовок результата',  
        'desc'  => 'укажите первый заголовок результата он будет выделен жирным шрифтом',  
        'id'    => 'result_h1', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
	array(  
        'label' => 'Второй заголовок результата',  
        'desc'  => 'укажите второй заголовок результата',  
        'id'    => 'result_h2', // даем идентификатор.
        'type'  => 'textarea'  // Указываем тип поля.
    ),
);

function show_my_case() {  
global $case_meta_fields; // Обозначим наш массив с полями глобальным
global $post;  // Глобальный $post для получения id создаваемого/редактируемого поста
// Выводим скрытый input, для верификации. Безопасность прежде всего!
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
 
    // Начинаем выводить таблицу с полями через цикл
    echo '<table class="form-table">';  
    foreach ($case_meta_fields as $field) {  
        // Получаем значение если оно есть для этого поля 
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // Начинаем выводить таблицу
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
					case 'textarea':  
					    echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					// Текстовое поле
					case 'text':  
					    echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" /> 
					        <br /><span class="description">'.$field['desc'].'</span>';  
					break;
					case 'select':  
					    echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';  
					    foreach ($field['options'] as $option) {  
					        echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';  
					    }  
					    echo '</select><br /><span class="description">'.$field['desc'].'</span>';  
					break;
                }
        echo '</td></tr>';  
    }  
    echo '</table>'; 
}

function save_my_case_fields($post_id) {  
    global $case_meta_fields;  // Массив с нашими полями
 
    // проверяем наш проверочный код 
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))   
        return $post_id;  
    // Проверяем авто-сохранение 
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
        return $post_id;  
    // Проверяем права доступа  
    if ('page' == $_POST['post_type']) {  
        if (!current_user_can('edit_page', $post_id))  
            return $post_id;  
        } elseif (!current_user_can('edit_post', $post_id)) {  
            return $post_id;  
    }  
 
    // Если все отлично, прогоняем массив через foreach
    foreach ($case_meta_fields as $field) {  
        $old = get_post_meta($post_id, $field['id'], true); // Получаем старые данные (если они есть), для сверки
        $new = $_POST[$field['id']];  
        if ($new && $new != $old) {  // Если данные новые
            update_post_meta($post_id, $field['id'], $new); // Обновляем данные
        } elseif ('' == $new && $old) {  
            delete_post_meta($post_id, $field['id'], $old); // Если данных нету, удаляем мету.
        }  
    } // end foreach  
}  
add_action('save_post', 'save_my_case_fields'); // Запускаем функцию сохранения





function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }           
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}



register_nav_menu('menu','Меню в шапке' );