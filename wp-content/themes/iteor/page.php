<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/style.css" />
        <script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/jquery.classyloader.min.js"></script>
        <script src="js/chart.js"></script>
        <script src="js/core.js"></script>
        <title>Chart</title>
    </head>
    <body>
        <div class="panel-body">
            <table class="j-campaigns_table table dataTable" style="width:100%!important;">
                <thead>
                <tr>
                    <th>CampID1</th>
                    <th>Agents</th>
                    <th>Answers</th>
                    <th>Calls</th>
                    <th>DNC</th>
                    <th>AA</th>
                    <th>NA</th>
                    <th>NI</th>
                    <th>HU</th>
                    <th>DEAD</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="agents"></div>
            <div class="panel-campaigns-chart" id="chart_div_campid" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_answers" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_calls" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_dnc" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_aa" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_na" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_ni" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_hu" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_dead" style="width:100%;height:40px;"></div>
        </div>
                <!--<div class="canvas-wrapper">
            <canvas class="agents"></canvas>
            <span class="canvas-label">Agents</span>
        </div>-->
        <!--<div class="graphics-wrapper">
            <div class="panel-campaigns-chart" id="chart_div_answers" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_calls" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_dnc" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_aa" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_na" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_ni" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_hu" style="width:100%;height:40px;"></div>
            <div class="panel-campaigns-chart" id="chart_div_dead" style="width:100%;height:40px;"></div>
        </div>-->
    </body>
</html>