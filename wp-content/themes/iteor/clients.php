<?php
/*
Template Name: Out Clients
*/
?>
<div class="companies-wrapper">
	<div class="companies-content center">
		<h2 class="standart-header">Our clients</h2>
		<div class="companies">
			<a href="#" class="company rails"></a>
			<a href="#" class="company lukoil"></a>
			<a href="#" class="company gasprom"></a>
			<a href="#" class="company sberbank"></a>
			<a href="#" class="company rostelekom"></a>
			<a href="#" class="company rosatom"></a>
			<a href="#" class="company transneft"></a>	
		</div>
		<div class="gerbs">
			<a href="#" class="gerb1"></a>
			<a href="#" class="gerb2"></a>
			<a href="#" class="gerb3"></a>
			<a href="#" class="gerb4"></a>
			<a href="#" class="gerb5"></a>
			<a href="#" class="gerb6"></a>
			<a href="#" class="gerb7"></a>
		</div>	
	</div>
</div>