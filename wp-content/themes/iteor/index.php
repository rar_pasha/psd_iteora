    <div class="main-nav">
        <?php get_header(); ?>  
    </div>

        <?php include('contactpopup.php'); ?> 

        <div class="wrapper">
            <div class="header-wrapper main-header-back">
                <div class="header center"> 
                    <div class="header-content">

                    <?php 
                        query_posts(array( 
                            'post_type' => 'main_header_content',
                            'showposts' => 7 
                        ) );  
                    ?>


                    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>


                        <div class="header-content-block">
                            <h1><?php 
                        $h1 = get_post_meta($post->ID, 'h1', true);
                        echo $h1;
                        ?></h1>
                            <h2><?php 
                        $h2 = get_post_meta($post->ID, 'h2', true);
                        echo $h2;
                        ?></h2>
                            <div class="button-group">
                                <a href="/case-studies">See Projects</a>
                                <a href="/about-us">Learn more</a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="services-wrapper">
                <div class="services center">
                    <h2 class="standart-header">Our Services</h2>
                    <div class="services-part"  data-columns>
                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/racket.jpg" alt="">    
                            </div>
                            <h3>Startup</h3>
                            <p>We help startups build and scale powerful apps</p>
                        </div>
                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/proger.jpg" alt="">    
                            </div>
                            <h3>Body leasing</h3>
                            <p>We make the process of outsourcing IT professionals easy and transparent for our clients</p>
                        </div>

                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/comp-faces.jpg" alt="">   
                            </div>
                            <h3>IT support</h3>
                            <p>We are your technical support. We cover a lot of risks. We will take care about everything</p>
                        </div>

                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/web-dev.jpg" alt="" class="web-dev">  
                            </div>
                            <h3>Web development</h3>
                            <p>We create powerful web platforms and tools for your business. eCommerce. Education. Telecom. Finance. Travel. Entertainment</p>
                        </div>

                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/phone.jpg" alt="">
                            </div>
                            <h3>Mobile apps</h3>
                            <p>We build high quality mobile apps saving your budget and your time</p>
                        </div>

                        <div class="services-blocks services-block1">
                            <div class="service-img-block">
                                <img src="<?php bloginfo('template_url'); ?>/img/consulting.jpg" alt="">   
                            </div>
                            <h3>It consulting</h3>
                            <p>We have a deep knowledge of IT infrastructure operations. We can help you to build the cost-effective, flexible, scalable and secure IT system your business deserves</p>
                        </div>      
                    </div>
                </div>
            </div>

            <?php include('projects.php') ?>

            <div class="team-wrapper">
                <div class="team center" data-columns>


                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 4 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

                </div>
            </div>
            
            <?php include('contact-us.php'); ?> 

    <?php get_footer(); ?>