$(function(){
    $("#header-language").styler({});

    $(".header-content").slick({
	  dots: true,
	  prevArrow:'<a class="left-arrow"></a>',
	  nextArrow:'<a class="right-arrow"></a>',
	  autoplay: true,
  	  autoplaySpeed: 5000
	});

	$(".project-slider-block").slick({
	  dots: true,
	  prevArrow:'<a class="left-arrow right-arr-project"></a>',
	  nextArrow:'<a class="right-arrow right-arr-project"></a>',
	  dotsClass:'slick-dots bottom-slick-dots',
	  autoplay: true,
  	  autoplaySpeed: 5000,
  	  pauseOnHover: false
	});

    $(".projects-slider  .slick-list").css({
    	'height': $(".projects-slider .slick-current").height() + 10
    });
	  $(".projects-slider .slick-arrow").css({
    	"top":"50%"
    });

	$(".project-slider-block").on('afterChange', function(event, slick, currentSlide, nextSlide){
	    $(".projects-slider  .slick-list").animate({
	    	'height': $(".projects-slider .slick-current").height() + 10
	    },300);
		  $(".projects-slider .slick-arrow").animate({
	    	"top":"50%"
	    },300);
	});

	if(typeof $('.projects-slider .slick-dots').offset() != 'undefined'){

	     function scrollActive(){

		  if($(window).scrollTop() > $('.projects-slider .slick-dots').offset().top - 150){
		  	$(".project-slider-block").slick('slickPause');	
		  }else{
		  	$(".project-slider-block").slick('slickPlay');		  	
		  }
	  	}
	  	
	    $(window).scroll(scrollActive);

	}

	$(".about-teammate-img,.header-slider-block-hire-block-img").hover(function(){
		$(this).find('.about-teammate-socials').show();
	},function(){
		$(this).find('.about-teammate-socials').hide();
	});

	$(".grid-item").each(function(){
		if($(this).index() == 0 || $(this).index() == 1){
			$(this).addClass("grid-item--width2");
		}else{
			$(this).addClass("grid-item--width1");			
		}
	});

	$(".platform-img img").each(function(){
		$(this).css({
			'margin-top': (189 - $(this).height())/2
		});
	});

	/*$(".project-slide-right-part").each(function(){
		$(this).css({
			'margin-top': (475 - $(this).height())/2
		});
	});
    
	$(".photo-gallery-block .grid-item img").each(function(){
		$(this).css({
			'position': 'relative',
			'top': (320 - $(this).height())/2
		});
	});*/


	$(".how-we-work-block").hover(function(){
		$(".how-we-work-block").removeClass('how-hovered');
		$(this).addClass('how-hovered');
	},function(){
		$(this).removeClass('how-hovered');
	});

	$(".how-we-work-block").on('click',function(){
		var tab = $(this).attr('data-tab');
		$('.how-we-work-block').removeClass('how-clicked');
		$(this).addClass('how-clicked');
		$(".how-we-works-tab").hide();
		$(".how-we-works-tab[data-tab = "+ tab + "]").fadeIn();
	});

	$('.case-block').hover(function(){
		$(this).find('.case-block-back').fadeIn();
	},function(){
		$(this).find('.case-block-back').fadeOut();
	});

	$(".case-block").on('click',function(){
		var tab = $(this).attr('data-tab');

		$('.case-block-tab-wrapper').hide();
		$('body').animate({
			'scrollTop': $(this).offset().top -130
		},400);


		if(!$(this).hasClass('case-block-clicked')){

			$(".case-block").removeClass('case-block-clicked');
			$(this).find('.case-block-back').fadeOut();
			$(this).addClass('case-block-clicked');

			$(".case-block").find('p').removeClass('clicked-case-p');
			$(this).find('p').addClass('clicked-case-p');

			$(this).parent().next('.case-block-tab-wrapper').fadeOut(100).find('.case-block-tab').fadeOut(100)

			$(this).parent().next('.case-block-tab-wrapper').fadeIn(100).find('.case-block-tab[data-tab = '+ tab +' ]').fadeIn(100);
		}else{

		$(".case-block").removeClass('case-block-clicked').find('p').removeClass('clicked-case-p');

		$(this).parent().next('.case-block-tab-wrapper').fadeOut(100).find('.case-block-tab').fadeOut(100)

		$(this).parent().next('.case-block-tab-wrapper').fadeOut(100).find('.case-block-tab[data-tab = '+ tab +' ]').fadeOut(100);	
		}
	});

	$('.about-companys-achievments a,.contact-popup-link,.get-quote-link').on('click',function(e){
		e.preventDefault();
		$('html').css({
			'overflow':'hidden'
		});
		$('.contact-us-popup-wrapper').fadeIn().css({
			'height': $('html').height() + 150,
			'width': $('html').width(),
			'background-color':'rgba(23,40,55,0.76)',
			'z-index':'100000',
			'overflow':'hidden',
			'top': ($(window).scrollTop())
		});
	});
	$('.close-popup').on('click',function(e){
		e.preventDefault();
		$('.contact-us-popup-wrapper').fadeOut();
		$('html').css({
			'overflow':'auto'
		});;
	});


	$(".logo a img").on('click',function(){
		$(".menu li").css("bacgkround","transparent !important");
	});

    $(".hire-img img").each(function(){
    	$(this).css({
    		'position':'relative',
    		'top':(195 - $(this).height())/2
    	});
    });

    $('.cases-block-row > div,.case-block-tab-content > div').each(function(){
    	$(this).attr('data-tab',$(this).index() + "");
    });

    $('.cases-block-row.row1 > div:first-child').addClass('case-block-clicked');
    $('.case-block-tab-content.row1 > div:first-child').addClass('case-block-tab-visible');


    $('.case-block-tab-top h2').each(function(){
    	if($(this).parent().next('.case-block-tab-top-block').find('img').size() == 0){
    		$(this).parent().css({
    			'width':'100%',
    			'display':'block',
    			'float':'none',
    			'margin-bottom': '85px'
    		}).end().css({
    			'width':'100%',
    			'text-align':'center',
    		});
    	}
    });


		/* ---------------------------------------------- /*
		 * Contact form ajax
		/* ---------------------------------------------- */

		$('#contact-us-page').submit(function(e) {

			e.preventDefault();

			var ap_name = $('#text-input').val();
			var ap_email = $('#email-input').val();
			var ap_message = $('#message ').val();
			var ap_phone = $('#phone-input').val();
			var formData = {
				'apply_name'       : ap_name,
				'apply_email'      : ap_email,
				'apply_message'    : ap_message,
				'apply_phone'    : ap_phone
			};

			 $.ajax({
					type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
					url         : '/wp-content/themes/iteor/apply_form.php', // the url where we want to POST
					data        : formData, // our data object
					success		: function(data){
									$('.contact-us-block-page').hide();
									$('.apply-success').fadeIn();
									console.log(data);
					}
			  });
		});

		$('#contact-us-popup').submit(function(e) {

			e.preventDefault();

			var ap_name = $('#text-input-popup').val();
			var ap_email = $('#email-input-popup').val();
			var ap_message = $('#message-popup').val();
			var ap_phone = $('#phone-input-popup').val();
			var formData = {
				'apply_name'       : ap_name,
				'apply_email'      : ap_email,
				'apply_message'    : ap_message,
				'apply_phone'    : ap_phone
			};

			 $.ajax({
					type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
					url         : '/wp-content/themes/iteor/apply_form.php', // the url where we want to POST
					data        : formData, // our data object
					success		: function(data){
									$('.contact-us-block-page').hide();
									$('.apply-success').fadeIn();
									console.log(data);				
					}
			  });
			});



});