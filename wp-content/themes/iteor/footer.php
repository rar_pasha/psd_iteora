        	<div class="footer-wrapper">
        		<div class="footer center">
        			<div class="footer-top-part">
        				<div class="footer-phone">
        					<div class="footer-number">
        						
        						<a href="tel:00 770 876 666"><span></span>00 770 876 666</a>
        						<p>Iteora LTD, Post address contact@iteora.com</p>
        					</div>
        				</div>
        				<div class="footer-socials">
        					<div class="footer-socials-block">
	        					<a href="https://www.facebook.com/Iteora-GmbH-320981384938042/" class="facebook"></a>
	    						<a href="https://www.linkedin.com/company/iteora-software" class="in"></a>	
        					</div>	
    						<p class="copyright">2016 &copy; All rights reserved. X-Company Pty Ltd.</p>
        				</div>
        			</div>
                    <div class="footer-bottom-part">
                        <div class="companies">
                            <span class="company company1"></span>
                            <span class="company company2"></span>
                            <span class="company company3"></span>
                            <span class="company company4"></span>
                            <span class="company company5"></span>
                        </div>
                        <div class="companies bottom-companies">
                            <span class="company company6"></span>
                            <span class="company company7"></span>
                            <span class="company company8"></span>
                            <span class="company company9"></span>
                            <span class="company company10"></span> 
                        </div>
                    </div>
        		</div>
        	</div>
		</div>
        <?php wp_footer(); ?>
    </body>
</html>