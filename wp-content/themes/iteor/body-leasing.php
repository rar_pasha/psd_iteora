<?php
/*
Template Name: Body Leasing
*/
?>
	<div class="body-leasing-nav">
    	<?php get_header(); ?>	
	</div>

        <?php include('contactpopup.php'); ?> 


        <div class="wrapper">
            <div class="header-wrapper body-leasing-header">
                <div class="header center"> 
			<div class="header-content">
			    <div class="header-slider-block header-slider-block-hire">
			        <h1>Hire Team</h1>
			        <div class="header-slider-block-hire-blocks">
			            <?php 
			                query_posts(array( 
			                    'post_type' => 'team',
			                    'showposts' => 5 
			                ) );  
			            ?>
			            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			                    <div class="header-slider-block-hire-block size1of5-block-hire">
			                        <div class="header-slider-block-hire-block-img">
			                            <a href="#" class="hire-img"><?php the_post_thumbnail(); ?></a>
			                            <div class="about-teammate-socials">
			                                <div class="about-teammate-socials-group">
			                                <a href="<?php $gitHub_link = get_post_meta($post->ID, 'gitHub_link', true);
			                            echo $gitHub_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/github.png" alt=""></a>
			                                <a href="<?php $in_link = get_post_meta($post->ID, 'in_link', true);
			                            echo $in_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/team-in-social.png" alt=""></a> 
			                                </div>
			                            </div>
			                        </div>
			                        <h4 class="header-slider-block-hire-block-name"><?php the_title();?></h4>
			                        <h5 class="header-slider-block-hire-block-position"><?php 
			                            $position = get_post_meta($post->ID, 'team_position', true);
			                            echo $position;?></h5>
			                        <h5 class="header-slider-block-hire-block-expirience"><?php 
			                            $exp = get_post_meta($post->ID, 'team_exp', true);
			                            echo $exp;?></h5>
			                    </div>
			                <?php endwhile; ?>
			                <!-- post navigation -->
			                <?php else: ?>
			                <!-- no posts found -->
			                <?php endif; ?>
			        </div>
			        <div class="button-group">
			            <a href="#" class="contact-popup-link">Hire developers</a>
			        </div>
			    </div>
			</div>  
		                </div>
            </div>
			
			<div class="body-leasing-wrapper">
				<div class="body-leasing-content center">
					<h1>Body leasing <span>/</span> IT support <span>/</span> Outsourcing</h1>
					<div class="body-leasing-content-top">
						<div class="leasing-img-group">
							<div class="leasing-img-group-left">
								<i></i>
								<h3>Body leasing</h3>
							</div>
							<div class="leasing-img-group-right">
								<i></i>
								<h3>it support</h3>				
							</div>
						</div>
						<p>We make the process of outsourcing IT professionals easy and transparent for our clients. For any purpose (mobile app, web app, CRM or some specific software development) body leasing is a good choice</p>
					</div>

					<div class="body-leasing-how">
						<h2 class="standart-header">How it works</h2>
						<div class="how-it-works-block">

							<div class="how-it-works-block-left-right">
								<div class="how-it-works-block-left-right-block how-it-works-block-left-right-block1">
									<div class="how-circle"><img src="<?php bloginfo('template_url'); ?>/img/how-circle1.png" alt=""></div>
									<h3>Profile</h3>
									<p>Define a profile of the candidate 
									(consult us, if you need help)</p>
								</div>
							
								<div class="how-it-works-block-left-right-block how-it-works-block-left-right-block1">
									<div class="how-circle"><img src="<?php bloginfo('template_url'); ?>/img/how-circle3.png" alt=""></div>
									<h3>Workplace</h3>
									<p>We arrange a workplace and communication</p>
								</div>
							</div>

							<div class="how-it-works-center-block">
								<div class="how-it-works-center-block-top">
									
								</div>

								<div class="how-it-works-center-block-center">
									<h3>your benefits</h3>	
									<ol>
										<li><span>FAST START</span></li>
										<li><span>FITS FOR ANY PROJECTS</span></li>
										<li><span>NO NEED FOR</span><span class="rest-text">LABOR CONTRACT</span></li>
										<li><span>GUARANTEES UNDER</span><span class="rest-text">GERMAN LAW</span></li>
									</ol>
								</div>

								<div class="how-it-works-center-block-bottom">
									
								</div>
							</div>

							<div class="how-it-works-block-left-right right-after-img">
								<div class="how-it-works-block-left-right-block how-it-works-block-left-right-block1">
									<div class="how-circle"><img src="<?php bloginfo('template_url'); ?>/img/how-circle2.png" alt=""></div>
									<h3>Search</h3>
									<p>We search for appropriate candidates in our database and send you their CV</p>
								</div>
							
								<div class="how-it-works-block-left-right-block how-it-works-block-left-right-block1">
									<div class="how-circle"><img src="<?php bloginfo('template_url'); ?>/img/how-circle4.png" alt=""></div>
									<h3>Meeting call</h3>
									<p>We arrange a call/meeting so that you can get to know our candidate</p>
								</div>
							</div>		
						</div>
						<div class="how-bottom-part">
							<div class="how-bottom-part-top">
								<div class="col-sm-4">
									<h3>Quick</h3>
									<p>The process takes a few days</p>
									<h3 class="top-offset">Efficient</h3>
									<p>You get exactly what you need</p>
								</div>
								<div class="col-sm-4">
									<h3>Quick</h3>
									<p>The candidate is available at once</p>
									<h3 class="top-offset">Efficient</h3>
									<p>You pay only after you are absolutely
									sure that the candidate fits you</p>
								</div>
								<div class="col-sm-4">
									<h3>Quick</h3>
									<p>The candidate is ready to start working immediately</p>
									<h3>Efficient</h3>
									<p>You don't need to sign a labor contract, you just pay fee to us</p>
								</div>
							</div>

                    		<?php include('ourtech.php'); ?> 
						</div>
					</div>
				</div>
			</div>
			
			<div class="about-companys-achievments-wrapper">
				<div class="about-companys-achievments center body-leasing-consultation">
					<h2>Need some consultation? </h2>
					<a href="#" class="contact-popup-link">Get in touch now!</a>
				</div>
			</div>

            <div class="team-wrapper">
                <div class="team center" data-columns>
                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 2 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

				</div>
			</div> 
			
				

            <?php include('contact-us.php'); ?>

    <?php get_footer(); ?>