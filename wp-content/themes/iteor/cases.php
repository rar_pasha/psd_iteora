<?php
/*
Template Name: Case studies
*/
?>
	<div class="cases-nav">
    	<?php get_header(); ?>	
	</div>

        <?php include('contactpopup.php'); ?>  

        <div class="wrapper">
            <div class="cases-wrapper">
            	<div class="cases-content">
            		<h2 class="standart-header">Case studies</h2>
            		<div class="cases-blocks">

						<div class="cases-block-row row1">
							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>
							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									if($key < 3){
										echo '<div class="case-block">
		            							<div class="case-block-back">
		            							</div>
		            							<p class="case-label"><span>'.$title.'</span></p>
		            						</div>';
									}

								}
							 ?>
						</div>

						<div class="case-block-tab-wrapper case-block-tab-visible row1">
							<div class="case-block-tab-content row1">

							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>

							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									$content = $value->post_content;
			                        $case_problem = get_post_meta($value->ID, 'case_problem', true);
			                        $thumbnail = get_the_post_thumbnail($value->ID);
									$res1_h = get_post_meta($value->ID, 'res1_h', true);
									$res2_h = get_post_meta($value->ID, 'res2_h', true);
									$res3_h = get_post_meta($value->ID, 'res3_h', true);
                                    $result_h1 = get_post_meta($value->ID, 'result_h1', true);
                                    $result_h2 = get_post_meta($value->ID, 'result_h2', true);

									if($key < 3){
										echo '<div class="case-block-tab">
									<div class="case-block-tab-top">
										<div class="case-block-tab-top-block case-block-tab-top-block-left">
											<h2>'. $title .' Case Study</h2>
											<p>'. $content .'</p>
										</div>
										<div class="case-block-tab-top-block">
											  '. $thumbnail .'
										</div>
									</div>
									<div class="case-tab-problem">
										<h2 class="case-tab-h">Problem</h2>
										<p class="case-tab-p">
											'.$case_problem.'	
			                            </p>
									</div>
									<div class="case-tab-solution">
										<h2 class="case-tab-h">Solution</h2>
										<div class="case-tab-sol-punkt">
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>1</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res1_h.'
													</h3>
													<p>
														'.$res1_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>2</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res2_h.'
													</h3>
													<p>
														'.$res2_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>3</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res3_h.'
													</h3>
													<p>
														'.$res3_descr.'
													</p>
												</div>
											</div>
										</div>	
									</div>

									<div class="case-tab-result">
										<h2 class="case-tab-h">Result</h2>
										<p class="case-tab-p-bold">
											'.$result_h1.'
										</p>
										<p class="case-tab-p">
											'.$result_h2.'
										</p>
									</div>
								</div>';
									}

								}
							 ?>

							</div>
							<div class="case-bot-sign"></div>
						</div>

						<div class="cases-block-row row2">
							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );


							?>
							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									if($key > 2 && $key < 6){
										echo '<div class="case-block">
		            							<div class="case-block-back">
		            							</div>
		            							<p class="case-label"><span>'.$title.'</span></p>
		            						</div>';
									}

								}
							 ?>

							
						</div>

						<div class="case-block-tab-wrapper row2">
							<div class="case-block-tab-content">

							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>

							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									$content = $value->post_content;
			                        $case_problem = get_post_meta($value->ID, 'case_problem', true);
			                        $thumbnail = the_post_thumbnail($value->ID);
									$res1_h = get_post_meta($value->ID, 'res1_h', true);
									$res2_h = get_post_meta($value->ID, 'res2_h', true);
									$res3_h = get_post_meta($value->ID, 'res3_h', true);
                                    $result_h1 = get_post_meta($value->ID, 'result_h1', true);
                                    $result_h2 = get_post_meta($value->ID, 'result_h2', true);

									if($key > 2 && $key < 6){
										echo '<div class="case-block-tab">
									<div class="case-block-tab-top">
										<div class="case-block-tab-top-block case-block-tab-top-block-left">
											<h2>'. $title .' Case Study</h2>
											<p>'. $content .'</p>
										</div>
										<div class="case-block-tab-top-block">
											  '. $thumbnail .'
										</div>
									</div>
									<div class="case-tab-problem">
										<h2 class="case-tab-h">Problem</h2>
										<p class="case-tab-p">
											'.$case_problem.'	
			                            </p>
									</div>
									<div class="case-tab-solution">
										<h2 class="case-tab-h">Solution</h2>
										<div class="case-tab-sol-punkt">
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>1</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res1_h.'
													</h3>
													<p>
														'.$res1_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>2</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res2_h.'
													</h3>
													<p>
														'.$res2_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>3</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res3_h.'
													</h3>
													<p>
														'.$res3_descr.'
													</p>
												</div>
											</div>
										</div>	
									</div>

									<div class="case-tab-result">
										<h2 class="case-tab-h">Result</h2>
										<p class="case-tab-p-bold">
											'.$result_h1.'
										</p>
										<p class="case-tab-p">
											'.$result_h2.'
										</p>
									</div>
								</div>';
									}

								}
							 ?>
							</div>
							<div class="case-bot-sign"></div>
						</div>

						<div class="cases-block-row row3">
							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );


							?>
							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									if($key > 5 && $key < 9){
										echo '<div class="case-block">
		            							<div class="case-block-back">
		            							</div>
		            							<p class="case-label"><span>'.$title.'</span></p>
		            						</div>';
									}

								}
							 ?>

							
						</div>

						<div class="case-block-tab-wrapper row3">
							<div class="case-block-tab-content">

							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>

							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									$content = $value->post_content;
			                        $case_problem = get_post_meta($value->ID, 'case_problem', true);
			                        $thumbnail = the_post_thumbnail($value->ID);
									$res1_h = get_post_meta($value->ID, 'res1_h', true);
									$res2_h = get_post_meta($value->ID, 'res2_h', true);
									$res3_h = get_post_meta($value->ID, 'res3_h', true);
                                    $result_h1 = get_post_meta($value->ID, 'result_h1', true);
                                    $result_h2 = get_post_meta($value->ID, 'result_h2', true);

									if($key > 5 && $key < 9){
										echo '<div class="case-block-tab">
									<div class="case-block-tab-top">
										<div class="case-block-tab-top-block case-block-tab-top-block-left">
											<h2>'. $title .' Case Study</h2>
											<p>'. $content .'</p>
										</div>
										<div class="case-block-tab-top-block">
											  '. $thumbnail .'
										</div>
									</div>
									<div class="case-tab-problem">
										<h2 class="case-tab-h">Problem</h2>
										<p class="case-tab-p">
											'.$case_problem.'	
			                            </p>
									</div>
									<div class="case-tab-solution">
										<h2 class="case-tab-h">Solution</h2>
										<div class="case-tab-sol-punkt">
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>1</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res1_h.'
													</h3>
													<p>
														'.$res1_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>2</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res2_h.'
													</h3>
													<p>
														'.$res2_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>3</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res3_h.'
													</h3>
													<p>
														'.$res3_descr.'
													</p>
												</div>
											</div>
										</div>	
									</div>

									<div class="case-tab-result">
										<h2 class="case-tab-h">Result</h2>
										<p class="case-tab-p-bold">
											'.$result_h1.'
										</p>
										<p class="case-tab-p">
											'.$result_h2.'
										</p>
									</div>
								</div>';
									}

								}
							 ?>
							</div>
							<div class="case-bot-sign"></div>
						</div>

						<div class="cases-block-row row4">
							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );


							?>
							<?php
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									if($key > 8 && $key < 12){
										echo '<div class="case-block">
		            							<div class="case-block-back">
		            							</div>
		            							<p class="case-label"><span>'.$title.'</span></p>
		            						</div>';
									}

								}
							 ?>

							
						</div>

						<div class="case-block-tab-wrapper row4">
							<div class="case-block-tab-content">

							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>

							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									$content = $value->post_content;
			                        $case_problem = get_post_meta($value->ID, 'case_problem', true);
			                        $thumbnail = the_post_thumbnail($value->ID);
									$res1_h = get_post_meta($value->ID, 'res1_h', true);
									$res2_h = get_post_meta($value->ID, 'res2_h', true);
									$res3_h = get_post_meta($value->ID, 'res3_h', true);
                                    $result_h1 = get_post_meta($value->ID, 'result_h1', true);
                                    $result_h2 = get_post_meta($value->ID, 'result_h2', true);
									if($key > 8 && $key < 12){
										echo '<div class="case-block-tab">
									<div class="case-block-tab-top">
										<div class="case-block-tab-top-block case-block-tab-top-block-left">
											<h2>'. $title .' Case Study</h2>
											<p>'. $content .'</p>
										</div>
										<div class="case-block-tab-top-block">
											  '. $thumbnail .'
										</div>
									</div>
									<div class="case-tab-problem">
										<h2 class="case-tab-h">Problem</h2>
										<p class="case-tab-p">
											'.$case_problem.'	
			                            </p>
									</div>
									<div class="case-tab-solution">
										<h2 class="case-tab-h">Solution</h2>
										<div class="case-tab-sol-punkt">
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>1</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res1_h.'
													</h3>
													<p>
														'.$res1_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>2</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res2_h.'
													</h3>
													<p>
														'.$res2_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>3</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res3_h.'
													</h3>
													<p>
														'.$res3_descr.'
													</p>
												</div>
											</div>
										</div>	
									</div>

									<div class="case-tab-result">
										<h2 class="case-tab-h">Result</h2>
										<p class="case-tab-p-bold">
											'.$result_h1.'
										</p>
										<p class="case-tab-p">
											'.$result_h2.'
										</p>
									</div>
								</div>';
									}

								}
							 ?>
							</div>
							<div class="case-bot-sign"></div>
						</div>

						<div class="cases-block-row row5">
							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );


							?>
							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									if($key > 11 && $key < 15){
										echo '<div class="case-block">
		            							<div class="case-block-back">
		            							</div>
		            							<p class="case-label"><span>'.$title.'</span></p>
		            						</div>';
									}

								}
							 ?>

							
						</div>

						<div class="case-block-tab-wrapper row5">
							<div class="case-block-tab-content">

							<?php

								global $post;

								$my_posts = new WP_Query;
								$myposts = $my_posts->query( array(
									'post_type' => 'case_content',
	        						'post_status'      => 'publish'
								) );

							?>

							<?php 
								foreach ($myposts as $key => $value){
									$title = $value->post_title;
									$content = $value->post_content;
			                        $case_problem = get_post_meta($value->ID, 'case_problem', true);
			                        $thumbnail = the_post_thumbnail($value->ID);
									$res1_h = get_post_meta($value->ID, 'res1_h', true);
									$res2_h = get_post_meta($value->ID, 'res2_h', true);
									$res3_h = get_post_meta($value->ID, 'res3_h', true);
                                    $result_h1 = get_post_meta($value->ID, 'result_h1', true);
                                    $result_h2 = get_post_meta($value->ID, 'result_h2', true);

									if($key > 11 && $key < 15){
										echo '<div class="case-block-tab">
									<div class="case-block-tab-top">
										<div class="case-block-tab-top-block case-block-tab-top-block-left">
											<h2>'. $title .' Case Study</h2>
											<p>'. $content .'</p>
										</div>
										<div class="case-block-tab-top-block">
											  '. $thumbnail .'
										</div>
									</div>
									<div class="case-tab-problem">
										<h2 class="case-tab-h">Problem</h2>
										<p class="case-tab-p">
											'.$case_problem.'	
			                            </p>
									</div>
									<div class="case-tab-solution">
										<h2 class="case-tab-h">Solution</h2>
										<div class="case-tab-sol-punkt">
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>1</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res1_h.'
													</h3>
													<p>
														'.$res1_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>2</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res2_h.'
													</h3>
													<p>
														'.$res2_descr.'
													</p>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="case-tab-sol-punkt-top">
													<div class="case-tab-sol-punkt-top-circle">
														<span>3</span>
													</div>
												</div>
												<div class="case-tab-sol-punkt-label">
													<h3>
														'.$res3_h.'
													</h3>
													<p>
														'.$res3_descr.'
													</p>
												</div>
											</div>
										</div>	
									</div>

									<div class="case-tab-result">
										<h2 class="case-tab-h">Result</h2>
										<p class="case-tab-p-bold">
											'.$result_h1.'
										</p>
										<p class="case-tab-p">
											'.$result_h2.'
										</p>
									</div>
								</div>';
									}

								}
							 ?>
							</div>
							<div class="case-bot-sign"></div>
						</div>          		
            		</div>
            	</div>
            </div>

			<div class="about-companys-achievments-wrapper">
				<div class="about-companys-achievments center body-leasing-consultation">
					<h2>Need some consultation? </h2>
					<a href="#" class="contact-popup-link">Get in touch now!</a>
				</div>
			</div>

            <div class="team-wrapper">
                <div class="team center" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 2 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

            	</div>
            </div>

            <?php include('/projects.php') ?>


            <?php include('/contact-us.php'); ?> 

    <?php get_footer(); ?>