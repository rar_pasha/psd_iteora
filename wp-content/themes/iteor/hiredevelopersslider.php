<?php
/*
Template Name: Header Slider About
*/
?>
<div class="header-content">
    <div class="header-slider-block header-slider-block-hire">
        <h1>Hire Team</h1>
        <div class="header-slider-block-hire-blocks">
            <?php 
                query_posts(array( 
                    'post_type' => 'team',
                    'showposts' => 5 
                ) );  
            ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div class="header-slider-block-hire-block size1of5-block-hire">
                        <div class="header-slider-block-hire-block-img">
                            <a href="#" class="hire-img"><?php the_post_thumbnail(); ?></a>
                            <div class="about-teammate-socials">
                                <div class="about-teammate-socials-group">
                                <a href="<?php $gitHub_link = get_post_meta($post->ID, 'gitHub_link', true);
                            echo $gitHub_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/github.png" alt=""></a>
                                <a href="<?php $in_link = get_post_meta($post->ID, 'in_link', true);
                            echo $in_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/team-in-social.png" alt=""></a> 
                                </div>
                            </div>
                        </div>
                        <h4 class="header-slider-block-hire-block-name"><?php the_title();?></h4>
                        <h5 class="header-slider-block-hire-block-position"><?php 
                            $position = get_post_meta($post->ID, 'team_position', true);
                            echo $position;?></h5>
                        <h5 class="header-slider-block-hire-block-expirience"><?php 
                            $exp = get_post_meta($post->ID, 'team_exp', true);
                            echo $exp;?></h5>
                    </div>
                <?php endwhile; ?>
                <!-- post navigation -->
                <?php else: ?>
                <!-- no posts found -->
                <?php endif; ?>
        </div>
        <div class="button-group">
            <a href="#" class="contact-popup-link">Hire developers</a>
            <a href="#">build team</a>
        </div>
    </div>
</div> 