<div class="tech-we-use">
	<h2 class="standart-header">Technologies we use</h2>
	<div class="platforms" data-columns>
		<div class="platform">
			<div class="img-platform">
				<img src="<?php bloginfo('template_url'); ?>/img/php.png" alt="">
			</div>
			<p>php</p>
		</div>
		<div class="platform">
			<div class="img-platform">
				<img src="<?php bloginfo('template_url'); ?>/img/mysql.png" alt="">
			</div>
			<p>MySQL</p>
		</div>
		<div class="platform">
			<div class="img-platform">
				<img src="<?php bloginfo('template_url'); ?>/img/python.png" alt="">
			</div>
			<p>Python</p>
		</div>
		<div class="platform">
			<div class="img-platform oracle">
				<img src="<?php bloginfo('template_url'); ?>/img/oracle.png" alt="">
			</div>
			<p>Oracle</p>
		</div>
		<div class="platform">
			<div class="img-platform">
				<img src="<?php bloginfo('template_url'); ?>/img/java.png" alt="">
			</div>
			<p>Java</p>
		</div>
		<div class="platform">
			<div class="img-platform react">
				<img src="<?php bloginfo('template_url'); ?>/img/react.png" alt="">
			</div>
			<p>React</p>
		</div>
	</div>
</div>