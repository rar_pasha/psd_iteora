<div class="about-team-wrapper">
  <div class="about-team-block center">
          <h2 class="standart-header">Our Team</h2>
          <div class="about-teammate-blocks" data-columns>

        <?php 
            query_posts(array( 
                'post_type' => 'team',
                'showposts' => 15 
            ) );  
        ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="about-teammate-teammate">
              <div class="about-teammate-img">
                <?php the_post_thumbnail(); ?>
                <div class="about-teammate-socials">
                  <div class="about-teammate-socials-group">
  <a href="<?php $gitHub_link = get_post_meta($post->ID, 'gitHub_link', true);
echo $gitHub_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/github.png" alt=""></a>
    <a href="<?php $in_link = get_post_meta($post->ID, 'in_link', true);
echo $in_link;?> "><img src="<?php bloginfo('template_url'); ?>/img/team-in-social.png" alt=""></a> 
                  </div>
                </div>
              </div>
              <div class="about-team-teammate-info">
                <h4 class="teammate-name"><?php the_title();?></h4>
                <h5 class="teammate-position"><?php 
                        $position = get_post_meta($post->ID, 'team_position', true);
                        echo $position;?>                                 
                    </h5>
              </div>
            </div>
        <?php endwhile; ?>
        <!-- post navigation -->
        <?php else: ?>
        <!-- no posts found -->
        <?php endif; ?>




          </div>        
  </div>
</div>