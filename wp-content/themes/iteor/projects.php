<?php
/*
Template Name: Projects
*/
?>
<div class="project-wrapper">
    <div class="project center">
        <h2 class="standart-header projects-header">Last performed projects</h2>
        <div class="projects-slider">

            <div class="project-slider-block">
                <?php 
                    query_posts(array( 
                        'post_type' => 'projects',
                        'showposts' => 6 
                    ) );  
                ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <div class="project-slide">
                        <h3 class="projects-slider-header"><?php 
                        $category_name = get_post_meta($post->ID, 'category', true);
                        echo $category_name;
                        ?></h3>
                        <div class="project-slide-left-part">
                            <h3><?php the_title();?></h3>
                            <p><?php echo content(40)?></p>
                            <div class="project-slide-left-part-view-button">
                                <a href="<?php bloginfo('template_url'); ?>/case-studies/">View case study</a>
                            </div>
                        </div>
                        <div class="project-slide-right-part">
                            <?php the_post_thumbnail(); ?>
                        </div>      
                    </div>
                <?php endwhile; ?>
                <!-- post navigation -->
                <?php else: ?>
                <!-- no posts found -->
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>