<?php
/*
Template Name: Mobile Development
*/
?>
    <div class="mobile-nav">
        <?php get_header(); ?>  
    </div>

        <?php include('contactpopup.php'); ?> 


        <div class="wrapper">
            <div class="header-wrapper mobile-back">
                <div class="header center">

                    <?php 
                        query_posts(array( 
                            'post_type' => 'mob_header_content',
                            'showposts' => 1 
                        ) );  
                    ?>


                    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>

                      <div class="mobile-header">
                            <div class="mobile-header-left">
                                <h1><?php 
                                $h1 = get_post_meta($post->ID, 'h1', true);
                                echo $h1;
                                ?></h1>
                                <h5><?php 
                                $h2 = get_post_meta($post->ID, 'h2', true);
                                echo $h2;
                                ?></h5> 
                                <a href="#" class="get-quote-link">Get a quote</a> 
                            </div>
                            <div class="mobile-header-right">
                                <?php the_post_thumbnail(); ?>
                            </div>
                      </div>

                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>   

                </div>
            </div>

            <div class="mobile-project-wrapper">
                <div class="mobile-project center">
                    <div class="mobile-project-img">
                        
                    </div>
                    <?php 
                        query_posts(array( 
                            'post_type' => 'projects',
                            'showposts' => 1 
                        ) );  
                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="mobile-project-info">
                        <h2 class="last-mob-proj">Last Project</h2>
                        <h3 class="mob-proj-name"><?php the_title();?></h3>
                        <p class="mob-proj-descr"><?php echo content(40)?></p>
                        <a href="<?php bloginfo('template_url'); ?>/case-studies/">View case study</a>
                    </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>
                </div>
            </div>

            <div class="what-we-do-wrapper">
                <div class="what-we-do center">
                    <h2 class="standart-header">What we do</h2>
                    <div class="what-platforms" data-columns>
                        <div class="what-platform">
                            <div class="platform-img andr">
                            </div>
                            <h3>Android</h3>
                            <p>There is only one way we write

                            our Android apps at Iteora –natively. We know the Android

                            platform and its devices

                            inside out. Our Android

                            development expertise rises

                            along with our customers

                            satisfaction.</p>
                        </div>  
                        <div class="what-platform">
                            <div class="platform-img ios">
                            </div>
                            <h3 class="transform-none">iOS</h3>
                            <p>At Iteora we build world class

                            iOS apps which capture

                            the attention of your users. 

                            As experts in Objective-C

                            and Swift, we have launched

                            dozens of iOS apps which are

                            currently used by thousands of

                            users.</p>
                        </div>   
                    </div>
                </div>
            </div>

            <div class="about-companys-achievments-wrapper">
                <div class="about-companys-achievments center body-leasing-consultation">
                    <h2>Need some consultation? </h2>
                    <a href="#" class="contact-popup-link">Get in touch now!</a>
                </div>
            </div>


            <?php include('projects.php') ?>

            <div class="team-wrapper">
                <div class="team center" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 2 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

                 </div>
            </div>

            <?php include('contact-us.php'); ?> 
            
    <?php get_footer(); ?>