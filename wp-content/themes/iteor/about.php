<?php
/*
Template Name: About Us
*/
?>
    <div class="about-nav">
        <?php get_header(); ?>  
    </div>

        <?php include('contactpopup.php'); ?>  


        <div class="wrapper">
            <div class="about-header">
                <div class="header center"> 
					<div class="header-content">

						<?php 
	                        query_posts(array( 
	                            'post_type' => 'about_header_content',
	                            'showposts' => 7 
	                        ) );  
	                    ?>


	                    <?php if ( have_posts()) : while ( have_posts() ) : the_post(); ?>


					    <div class="header-slider-block header-slider-block-about">
					        <h1><?php 
	                        $h1 = get_post_meta($post->ID, 'h1', true);
	                        echo $h1;
	                        ?></h1>
					        <h2><?php 
	                        $h2 = get_post_meta($post->ID, 'h2', true);
	                        echo $h2;
	                        ?></h2>
					        <h3><?php 
	                        $h3 = get_post_meta($post->ID, 'h3', true);
	                        echo $h3;
	                        ?></h3>
					    </div>
					    <?php endwhile; ?>
	                    <!-- post navigation -->
	                    <?php else: ?>
	                    <!-- no posts found -->
	                    <?php endif; ?>
					</div> 
                </div>
            </div>
			
			<div class="about-us-team">
				<h2 class="standart-header">Who we are</h2>
				<div class="center about-us-team-content">
					<div class="about-us-team-content-block-1">
					    <div class="about-us-team-content-block-1-top-part">
					    	 <div class="developers-count">
					    	     <h3>100</h3>	
					    	     <p>Developers
									Designers
									Project Managers
									Business Analytics
									Qa</p>
					    	 </div>
					    	 <div class="about-us-est">
					    	 	<h3>2008</h3>
					    	 </div>
					    </div>
					</div>
					<div class="about-us-team-content-block-2">
						<h3>markets</h3>
						<div class="about-us-team-market">
							<ul>
								<li><span>Travel</span></li>
								<li><span>Technology</span></li>
								<li><span>Education</span></li>
								<li><span>Health</span></li>
								<li><span>Telecom</span></li>
								<li><span>Marketing</span></li>
							</ul>	
							<div class="about-graphic">
								<img src="<?php bloginfo('template_url'); ?>/img/graphic.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="about-us-team-content-block-3">
						<div class="about-us-team-content-block-3-top">
							<img src="<?php bloginfo('template_url'); ?>/img/200+.png" alt="">
						</div>
						<div class="about-us-team-content-block-3-bottom">
							<h3>100+</h3>
							<p>Native mobile apps</p>
						</div>
					</div>
				</div>
			</div>

            <?php include('our-team.php') ?>

			<div class="about-companys-achievments-wrapper">
				<div class="about-companys-achievments center">
					<h2>100 members, 4 officed, 300+ projects</h2>
				</div>
			</div>
			
			<div class="photo-gallery-wrapper">
				<div class="photo-gallery center">
					<h2>Photo Gallery</h2>
					<div class="photo-gallery-block">
                    <?php 
                        query_posts(array( 
                            'post_type' => 'gallery',
                            'showposts' => 5 
                        ) );  
                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				    	<div class="grid-item"><?php the_post_thumbnail(); ?></div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>	
					</div>
				</div>
			</div>


            <?php include('contact-us.php'); ?> 



    <?php get_footer(); ?>


