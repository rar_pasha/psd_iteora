<div class="contact-us-popup-wrapper">
	<div class="contact-us-popup">
		<a href="#" class="close-popup">Close</a>
		<h2>Contact Us</h2>
		<h4>Need service or support? Start your request online and we’ll find you a solution.</h4>
        <div class="contact-us-block-page">
			<div class="contact-us-form-popup">
				<form name="contact-us-form-popup" id="contact-us-popup" class="contact-us-form-popup">
					<div class="col-sm-6">
		                <div class="col-sm-12">
		                    <label for="text-input" class="name-label">Name</label>
		                    <input type="text" name="usrname-popup" id="text-input-popup" class="text-input-popup">
		                </div>
		                <div class="col-sm-12">
		                    <label for="email-input" class="email-label">Email</label>
		                    <input type="email"  name="usremail-popup" id="email-input-popup" class="email-input-popup" required>
		                </div>
		                <div class="col-sm-12">
		                    <label for="phone-input" class="phone-label">Phone</label>
		                    <input type="tel" name="usrtel-popup" id="phone-input-popup" class="phone-input-popup">
		                </div>
					</div>
					<div class="col-sm-6 message-part-popup">
		                <div class="col-sm-12">
		                    <label for="message" class="message-label">Message</label>
		                    <textarea name="message-popup" id="message-popup" class="message"></textarea>
		                </div>
					</div>
					<div class="button-submit-block col-sm-12">
		                <input type="submit" class="button-submit" value="Send">
		           </div>
				</form>
			</div>
		</div>
		<div class="apply-success">
            <h2>Your request was succesfully accepted!</h2>
        </div>
	</div>
</div>