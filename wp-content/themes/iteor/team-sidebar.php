                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews',
                            'showposts' => 4 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $reviews_position = get_post_meta($post->ID, 'reviews_position', true);
                                    echo $reviews_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

                    <!--<div class="team-block">
                        <div class="team-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/team-img.png" alt="">
                        </div>
                        <div class="team-text">
                            <p class="descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p class="n-s">Name Surname</p>
                            <p class="pos">Position</p> 
                        </div>
                    </div>

                    <div class="team-block">
                        <div class="team-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/team-img.png" alt="">
                        </div>
                        <div class="team-text">
                            <p class="descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p class="n-s">Name Surname</p>
                            <p class="pos">Position</p> 
                        </div>
                    </div>

                    <div class="team-block">
                        <div class="team-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/team-img.png" alt="">
                        </div>
                        <div class="team-text">
                            <p class="descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p class="n-s">Name Surname</p>
                            <p class="pos">Position</p> 
                        </div>
                    </div>

                    <div class="team-block">
                        <div class="team-img">
                            <img src="<?php bloginfo('template_url'); ?>/img/team-img.png" alt="">
                        </div>
                        <div class="team-text">
                            <p class="descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p class="n-s">Name Surname</p>
                            <p class="pos">Position</p> 
                        </div>
                    </div>-->
    