<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <title><?php bloginfo('name'); ?></title>
        <?php wp_head(); ?>
        <style>
            .left-arrow{
                background:url("<?php bloginfo('template_url'); ?>/img/left-arr.png") no-repeat 0 0;
                left:-28px;
            }
            .left-arrow:hover{
                background:url("<?php bloginfo('template_url'); ?>/img/left-arr-h.png") no-repeat 0 0;
                cursor:pointer;
            }

            .right-arrow{
                background:url("<?php bloginfo('template_url'); ?>/img/right-arr.png") no-repeat 0 0;
                right:-28px;
            }
            .right-arrow:hover{
                background:url("<?php bloginfo('template_url'); ?>/img/right-arr-h.png") no-repeat 0 0;
                cursor:pointer;
            }
        </style>
    </head>
    <body>
        <div class="header-top-wrapper">
            <div class="header-top">
                <div class="logo"> 
                    <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt=""></a>
                </div>
                <div class="lang">
                    <select name="lang" id="header-language">
                        <option>en</option>
                        <option>ru</option>
                        <option>de</option>
                    </select>
                </div>
                <a href="<?php echo home_url(); ?>" class="home-link"></a>
                <div class="header-nav">
                    <?php wp_nav_menu( array('theme_location' => 'menu') ); ?>

                </div>
            </div>              
        </div>