<?php
/*
Template Name: Startup
*/
?>
	<div class="startup-nav">
    	<?php get_header(); ?>	
	</div>

        <?php include('contactpopup.php'); ?> 

        	
        <div class="wrapper">
            <div class="header-wrapper startup-header">
                <div class="startup-header header center"> 
			        <h1>Startups</h1>
			        <h2>We help startups build and scale powerful apps</h2>
			        <div class="startup-btn-group">
			        	<a href="#" class="get-quote-link">Get a Qoute</a>
			        </div>   
			        <div class="startup-design-blocks" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'startups_content',
                            'showposts' => 3 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			        	<div class="startup-design-block">
			        		<h3><?php the_title();?></h3>
			        		<?php the_content();?>
			        	</div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>
			        </div>
                </div>
            </div>
			
			<div class="dev-team-wrapper">
				<div class="dev-team center">
				    <h2>Iteora <span>dev team</span> <span class="span-vs">vs</span> <span>inhouse</span> dev team</h2>	
				    <h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h3>
					<div class="startup-markets-overview">
						<h3>Startup market overview</h3>
						<div class="dev-teams-compare">
							<div class="teams-compare-iteora col-sm-6">

								<div class="teams-compare-top">
									<div class="teams-compare-columns teams-compare-iteora-top">
										<div class="teams-compare-columns">
											<h2 class='compare-header'>Percent failed after four years <span>(by industry)</span></h2>
											<img src="<?php bloginfo('template_url'); ?>/img/iteora-chart.png" alt="" class='compare-chart1'>
										</div>
									</div>
								</div>

								<div class="teams-compare-content">
									<div class="teams-compare-content-header teams-compare-content-header-iteora dev-team">
										<h2><span>Iteora</span> dev team</h2>
									</div>
									<div class="teams-compare-content-blocks">
										<div class="teams-compare-content-block-header">
											<h4>Helps you to build and train your technical team</h4>
										</div>
										<div class="teams-compare-content-block compare-plus">
											<h3>Is always on time</h3>
											<h4>We always meet client deadlines. We have a great team and amazing capacities (over 100+ employees) which help us to deliver on time or even earlier. </h4>
										</div>
										<div class="teams-compare-content-block compare-plus">
											<h3>Provides you with quality product</h3>
											<h4>Upon completion of the work, we provide a full-cycle of software testing. Our QA department would rather die than allow us to provide you with a low-quality product. We provide a lifetime guarantee, which means that we will fix bugs and problems free of charge. </h4>
										</div>
										<div class="teams-compare-content-block compare-plus">
											<h3>Has all necessary resources and partners</h3>
											<h4>Iteora has a base prepared for successful project implementation. An experienced team, a large variety of tech partners (payment systems, APIs, servers, etc) and 15 years of business expertise</h4>
										</div>
										<div class="teams-compare-content-block compare-plus">
											<h3>Understands the startup mentality</h3>
											<h4>Time, quality and perfect management. That is what we provide, plus everything you need at the investment stage.  </h4>
										</div>
									</div>
								</div>

							</div>
							<div class="teams-compare-inhouse col-sm-6">
								<div class="teams-compare-top">
									<div class="teams-compare-columns teams-compare-iteora-top">
										<div class="teams-compare-columns">
											<h2 class='compare-header'>Major cause of failure</h2>
											<img src="<?php bloginfo('template_url'); ?>/img/startup-chart.png" alt="" class='compare-chart2'>
										</div>
									</div>
								</div>

								<div class="teams-compare-content">
									<div class="teams-compare-content-header teams-compare-content-header-inhouse dev-team">
										<h2><span>Startup</span> dev team</h2>
									</div>
									<div class="teams-compare-content-blocks">
										<div class="teams-compare-content-block-header">
											<h4>Spends hours hunting a good fit for a team</h4>
										</div>
										<div class="teams-compare-content-block compare-minus">
											<h3>Misses deadlines</h3>
											<h4>A startup in-house team usually consists of < 10 enthusiastic people. Even going hard at it, you don’t always have enough capacity to be on time. </h4>
										</div>
										<div class="teams-compare-content-block compare-minus">
											<h3>Produces unsatisfactory products</h3>
											<h4>The product is released, customers have purchased, but they are constantly facing problems when using your product. The team cannot provide a proper on-going operation week after week, month after month. Customers will not stand for this and eventually leave.</h4>
										</div>
										<div class="teams-compare-content-block compare-minus">
											<h3>Takes a lot of time to find a good fit for the project</h3>
											<h4>Time is slipping away while you’re trying to find an experienced specialist, the right and cost-effective technological partner, good server and etc. </h4>
										</div>
										<div class="teams-compare-content-block compare-minus">
											<h3>Has bad prioritizing</h3>
											<h4>As is always the case, at the beginning you are not suret what you should do first or how to assign priorities to the pile of tasks stacked up in your backlog. Time is passing by while you’re trying to figure it out. </h4>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>


            <?php include('projects.php') ?>

            
			<div class="about-companys-gray-back">
				<div class="about-companys-achievments-wrapper about-companys-achievments-wrapper-bigger-margin">
					<div class="about-companys-achievments center">
						<h2>We are developers to whom you can trust</h2>
					</div>
				</div>
			</div>

            <div class="team-wrapper">
                <div class="team center" data-columns>

                    <?php 
                        query_posts(array( 
                            'post_type' => 'reviews_content',
                            'showposts' => 2 
                        ) );  

                    ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <div class="team-block">
                            <div class="team-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="team-text">
                                <p class="descr"><?php the_content();?></p>
                                <p class="n-s"><?php the_title();?></p>
                                <p class="pos"><?php 
                                    $revs_position = get_post_meta($post->ID, 'revs_position', true);
                                    echo $revs_position;
                                ?></p> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <!-- post navigation -->
                    <?php else: ?>
                    <!-- no posts found -->
                    <?php endif; ?>

            	</div>
            </div>
            <?php include('contact-us.php'); ?> 



    <?php get_footer(); ?>