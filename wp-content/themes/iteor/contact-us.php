<?php
/*
Template Name: Contact Us
*/
?>
<div class="contact-wrapper">
    <div class="contact center">
        <div class="contact-us-block ">
            <h2>Contact Us</h2>
            <div class="contact-us-block-page">
                <form name="contact-us-form-page" id="contact-us-page" class="contact-us-form">
                    <div class="col-sm-4">
                        <label for="text-input" class="name-label">Name</label>
                        <input type="text"  name="usrname" id="text-input" class="text-input">
                    </div>
                    <div class="col-sm-4">
                        <label for="email-input" class="email-label">Email</label>
                        <input type="email"  name="usremail" id="email-input" class="email-input" required>
                    </div>
                    <div class="col-sm-4">
                        <label for="phone-input" class="phone-label">Phone</label>
                        <input type="phone"  name="usrtel" id="phone-input" class="phone-input">
                    </div>
                    <div class="col-sm-12 message-block">
                        <label for="message" class="message-label">Message</label>
                        <textarea name="message" class="message" id="message"></textarea>
                    </div>
                    <div class="button-submit-block">
                        <input type="submit" class="button-submit" value="Send">
                    </div>
                </form>
            </div>
            <div class="apply-success">
                <h2>Your request was succesfully accepted!</h2>
            </div>
        </div>
    </div>
</div>